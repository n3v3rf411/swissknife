import com.sprasia.jenkins.cache.Caches

def call(cacheConfigurations = [], Closure body) {
    if (!(cacheConfigurations instanceof Collection)) {
        cacheConfigurations = [cacheConfigurations]
    }

    def caches = cacheConfigurations.collect {
        if (!(it instanceof String)) {
            return it
        }

        if (Caches.STANDARD.containsKey(it)) {
            def path = Caches.getMountPathForStandardCache(WORKSPACE, it)
            sh "mkdir -p ${path}"
            return [$class: 'ArbitraryFileCache', includes: '**/*', path: path, standard: true]
        }

        // ignore
        echo "Ignoring unsupported cache \"$it\""
    }

    cache(maxCacheSize: 1000, caches: caches) {
        body()
    }

    sh "rm -Rf " + Caches.getMountPath(WORKSPACE)
}
