import com.sprasia.jenkins.cache.Caches

import java.util.regex.Matcher

def call(String str) {
    def expanded = str
    Caches.STANDARD.keySet().each {
        expanded = expanded.replaceAll("\\[${it}\\]", Matcher.quoteReplacement(Caches.STANDARD[it].path))
        expanded = expanded.replaceAll("\\[${it}\\.mount\\]", Matcher.quoteReplacement(Caches.getMountPathForStandardCache(WORKSPACE, it)))
    }
    return expanded
}
