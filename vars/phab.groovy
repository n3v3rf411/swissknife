import com.sprasia.jenkins.phabricator.conduit.ConduitAPIClient
import com.sprasia.jenkins.phabricator.conduit.HarbormasterClient

// vars/phab.groovy
class phab {
    private def script

    def build

    // Deprecated. Use build instead.
    def diff

    def set(script) {
        this.script = script
        build = new phabBuild(script)
        diff = build
    }

    static class phabBuild {
        private def script
        private def env
        private def hasSentBuildUri

        phabBuild(script) {
            this.script = script
            this.env = script.env
            this.hasSentBuildUri = false
        }

        private HarbormasterClient createHarbormasterClient() {
            String url = env.CONDUIT_URI ?: "https://phabricator.sprasia.jp/"

            String token = env.CONDUIT_TOKEN
            assert token: 'CONDUIT_TOKEN must be set in environment'

            ConduitAPIClient conduit = new ConduitAPIClient(url, token)
            return new HarbormasterClient(conduit)
        }

        private void doSendMessage(HarbormasterClient client, String phid, boolean pass) {
            String target = phid ?: env.PHID
            assert target: 'PHID must be passed or set in environment'

            doSendBuildUri(client, phid)

            def result = client.sendHarbormasterMessage(target, pass)
            if (result["error_info"]) {
                script.error String.format("Error from Harbormaster: %s", result["error_info"])
            }
        }

        private void doSendBuildUri(HarbormasterClient client, String phid) {
            if (this.hasSentBuildUri) {
                return
            }

            String target = phid ?: env.PHID
            assert target: 'PHID must be passed or set in environment'

            String buildUrl = env.BUILD_URL

            def result = client.sendHarbormasterUri(target, buildUrl)
            if (result["error_info"]) {
                script.error String.format("Harbormaster declined URI artifact: %s", result["error_info"])
            }

            this.hasSentBuildUri = true
        }

        def setUri(String phid = null) {
            def client = createHarbormasterClient()
            doSendBuildUri(client, phid)
        }

        def success(String phid = null) {
            def client = createHarbormasterClient()
            doSendMessage(client, phid, true)
        }

        def failure(String phid = null) {
            def client = createHarbormasterClient()
            doSendMessage(client, phid, false)
        }

        def aborted(String phid = null) {
            def client = createHarbormasterClient()
            doSendMessage(client, phid, false)
        }
    }
}
