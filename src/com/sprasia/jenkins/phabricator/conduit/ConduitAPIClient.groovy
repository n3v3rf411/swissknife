package com.sprasia.jenkins.phabricator.conduit

import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import org.apache.http.HttpStatus
import org.apache.http.HttpResponse
import org.apache.http.NameValuePair
import org.apache.http.client.ClientProtocolException
import org.apache.http.client.entity.UrlEncodedFormEntity
import org.apache.http.client.methods.HttpPost
import org.apache.http.client.methods.HttpUriRequest
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.http.message.BasicNameValuePair

class ConduitAPIClient {

    private static final String API_TOKEN_KEY = "token"
    private static final String CONDUIT_METADATA_KEY = "__conduit__"

    private final String conduitURL
    private final String conduitToken

    ConduitAPIClient(String conduitURL, String conduitToken) {
        this.conduitURL = conduitURL
        this.conduitToken = conduitToken
    }

    /**
     * Call the conduit API of Phabricator
     *
     * @param action Name of the API call
     * @param params The data to send to Harbormaster
     * @return The result as a JSONObject
     * @throws IOException If there was a problem reading the response
     * @throws ConduitAPIException If there was an error calling conduit
     */
    def perform(String action, params) throws IOException, ConduitAPIException {
        CloseableHttpClient client = HttpClientBuilder.create().build()
        HttpUriRequest request = createRequest(action, params)

        HttpResponse response
        try {
            response = client.execute(request)
        } catch (ClientProtocolException e) {
            throw new ConduitAPIException(e.getMessage())
        }

        InputStream responseBody = response.getEntity().getContent()

        if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
            throw new ConduitAPIException(responseBody.toString(), response.getStatusLine().getStatusCode())
        }

        JsonSlurper jsonParser = new JsonSlurper()
        return jsonParser.parse(responseBody)
    }

    /**
     * Post a URL-encoded "params" key with a JSON-encoded body as per the Conduit API
     *
     * @param action The name of the Conduit method
     * @param params The data to be sent to the Conduit method
     * @return The request to perform
     * @throws UnsupportedEncodingException when the POST data can't be encoded
     * @throws ConduitAPIException when the conduit URL is misconfigured
     */
    HttpUriRequest createRequest(String action, params) throws UnsupportedEncodingException,
            ConduitAPIException {
        HttpPost post
        try {
            post = new HttpPost(
                    new URL(new URL(new URL(conduitURL), "/api/"), action).toURI()
            )
        } catch (MalformedURLException | URISyntaxException e) {
            throw new ConduitAPIException(e.getMessage())
        }

        def conduitParams = [:]
        conduitParams[API_TOKEN_KEY] = conduitToken
        params[CONDUIT_METADATA_KEY] = conduitParams

        List<NameValuePair> formData = new ArrayList<NameValuePair>()
        formData.add(new BasicNameValuePair("params", JsonOutput.toJson(params)))

        UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formData, "UTF-8")
        post.setEntity(entity)

        return post
    }
}
