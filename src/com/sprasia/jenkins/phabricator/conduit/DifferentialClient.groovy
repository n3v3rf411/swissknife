package com.sprasia.jenkins.phabricator.conduit

import groovy.json.JsonOutput

/**
 * DifferentialClient handles all interaction with conduit/arc for differentials
 */
class DifferentialClient {

    private final String diffID
    private final ConduitAPIClient conduit

    DifferentialClient(String diffID, ConduitAPIClient conduit) {
        this.diffID = diffID
        this.conduit = conduit
    }

    /**
     * Posts a comment to a differential
     *
     * @param revisionID the revision ID (e.g. "D1234" without the "D")
     * @param message the content of the comment
     * @param silent whether or not to trigger an email
     * @param action phabricator comment action, e.g. 'resign', 'reject', 'none'
     * @return the Conduit API response
     * @throws IOException if there is a network error talking to Conduit
     * @throws ConduitAPIException if any error is experienced talking to Conduit
     */
    def postComment(String revisionID, String message, boolean silent, String action) throws IOException,
            ConduitAPIException {
        def params = [
                revision_id: revisionID,
                action: action,
                message: message,
                silent: silent
        ]

        return this.callConduit("differential.createcomment", params)
    }

    /**
     * Fetch a differential from Conduit
     *
     * @return the Conduit API response
     * @throws IOException if there is a network error talking to Conduit
     * @throws ConduitAPIException if any error is experienced talking to Conduit
     */
    def fetchDiff() throws IOException, ConduitAPIException {
        def params = [ids: [ diffID ]]
        def query = this.callConduit("differential.querydiffs", params)
        def response = query["result"]
        if (!response) {
            throw new ConduitAPIException(
                    String.format("No 'result' object found in conduit call: %s",
                            JsonOutput.toJson(query)))
        }

        def diff = response[diffID]
        if (!diff) {
            throw new ConduitAPIException(
                    String.format("Unable to find '%s' key in 'result': %s",
                            diffID,
                            JsonOutput.toJson(response)))
        }

        return diff
    }

    /**
     * Sets a sendHarbormasterMessage build status
     *
     * @param phid Phabricator object ID
     * @param pass whether or not the build passed
     * @param unitResults the results from the unit tests
     * @param coverage the results from the coverage provider
     * @return the Conduit API response
     * @throws IOException if there is a network error talking to Conduit
     * @throws ConduitAPIException if any error is experienced talking to Conduit
     */
    def sendHarbormasterMessage(
            String phid, boolean pass) throws ConduitAPIException, IOException {
        return new HarbormasterClient(conduit).sendHarbormasterMessage(phid, pass)
    }

    /**
     * Uploads a uri as an 'artifact' for Harbormaster to display
     *
     * @param phid Phabricator object ID
     * @param buildUri Uri to display, presumably the jenkins builds
     * @return the Conduit API response
     * @throws IOException if there is a network error talking to Conduit
     * @throws ConduitAPIException if any error is experienced talking to Conduit
     */
    def sendHarbormasterUri(String phid, String buildUri) throws ConduitAPIException, IOException {
        return new HarbormasterClient(conduit).sendHarbormasterUri(phid, buildUri)
    }

    /**
     * Post a comment on the differential
     *
     * @param revisionID the revision ID (e.g. "D1234" without the "D")
     * @param message the string message to post
     * @return the Conduit API response
     * @throws IOException if there is a network error talking to Conduit
     * @throws ConduitAPIException if any error is experienced talking to Conduit
     */
    def postComment(String revisionID, String message) throws ConduitAPIException, IOException {
        return postComment(revisionID, message, true, "none")
    }

    /**
     * Fetch the commit message for the revision. This isn't available on the diff, so it requires a separate query.
     *
     * @param revisionID The ID of the revision, e.g. for "D123" this would be "123"
     * @return A \n-separated string of the commit message
     * @throws ConduitAPIException
     * @throws IOException
     */
    String getCommitMessage(String revisionID) throws ConduitAPIException, IOException {
        def params = [revision_id: revisionID]
        def query = callConduit("differential.getcommitmessage", params)

        // NOTE: When you run this with `arc call-conduit dfferential.getcommitmessage` (from the command-line),
        // it comes back as "response". But it's "result" when running through this conduit API.
        return query["result"]
    }

    protected def callConduit(String methodName, def params) throws ConduitAPIException, IOException {
        return conduit.perform(methodName, params)
    }
}
