package com.sprasia.jenkins.phabricator.conduit

class ConduitAPIException extends Exception {

    public final int code

    ConduitAPIException(String message) {
        super(message)
        this.code = 0
    }

    ConduitAPIException(String message, int code) {
        super(message)
        this.code = code
    }
}
