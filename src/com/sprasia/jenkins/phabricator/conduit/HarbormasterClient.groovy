package com.sprasia.jenkins.phabricator.conduit

class HarbormasterClient {

    private final ConduitAPIClient conduit

    HarbormasterClient(ConduitAPIClient conduit) {
        this.conduit = conduit
    }

    /**
     * Sets a sendHarbormasterMessage build status
     *
     * @param phid Phabricator object ID
     * @param pass whether or not the build passed
     * @return the Conduit API response
     * @throws IOException if there is a network error talking to Conduit
     * @throws ConduitAPIException if any error is experienced talking to Conduit
     */
    def sendHarbormasterMessage(
            String phid,
            boolean pass) throws ConduitAPIException, IOException {

        def params = [
            type: pass ? "pass" : "fail",
            buildTargetPHID: phid
        ]

        return conduit.perform("harbormaster.sendmessage", params)
    }

    /**
     * Uploads a uri as an 'artifact' for Harbormaster to display
     *
     * @param phid Phabricator object ID
     * @param buildUri Uri to display, presumably the jenkins builds
     * @return the Conduit API response
     * @throws IOException if there is a network error talking to Conduit
     * @throws ConduitAPIException if any error is experienced talking to Conduit
     */
    def sendHarbormasterUri(String phid, String buildUri) throws ConduitAPIException, IOException {
        def artifactData = [
            uri: buildUri,
            name: "Jenkins",
            "ui.external": true
        ]

        def params = [
            buildTargetPHID: phid,
            artifactKey: "jenkins.uri",
            artifactType: "uri",
            artifactData: artifactData
        ]

        return conduit.perform("harbormaster.createartifact", params)
    }
}
