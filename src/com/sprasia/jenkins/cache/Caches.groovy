package com.sprasia.jenkins.cache

class Caches {

    public static final STANDARD = [
        "npm": [
            "path": ".npm",
        ],
        "composer": [
            "path": ".composer/cache/files",
        ],
        "yarn": [
            "path": ".cache/yarn",
        ],
        "pip": [
            "path": ".cache/pip",
        ],
        "maven": [
            "path": ".m2",
        ],
        "sonar": [
            "path": ".sonar",
        ],
    ]

    static String getMountPath(String basePath) {
        return "${basePath}/.jenkinscache"
    }

    static String getMountPathForStandardCache(String basePath, String cacheName) {
        return getMountPath(basePath) + "/" + STANDARD[cacheName].path
    }

}
